package g30126.Iakkel.Arthur.l9.e4;

public class Main {
	public static void main(String[] args) throws Exception{
		CarFactory carFactory=new CarFactory();
		Car car=carFactory.createCar("Volkswagen", 10000);
		Car car2=carFactory.createCar("Bemveu", 15000);
		carFactory.loadCar(car, "car1.dat");
		carFactory.loadCar(car2, "car2.dat");
		
		Car car3=carFactory.removeCar("car1.dat");
		System.out.println(car3.toString());
	}
}
