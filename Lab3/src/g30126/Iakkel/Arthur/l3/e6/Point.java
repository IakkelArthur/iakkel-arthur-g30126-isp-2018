package g30126.Iakkel.Arthur.l3.e6;

public class Point {
	private int x,y;
	Point(int x, int y){
		this.x=x;
		this.y=y;
	}
	
	public void set(int x,int y) {
		this.x=x;
		this.y=y;
	}
	
	public int getx() {
		return x;
	}
	public int gety() {
		return y;
	}
	
	public String toString() {
		return "(" +x+ "," +y+ ")";
	}
	
	public double distance(Point a) {
		return Math.sqrt((x-a.getx())*(x-a.getx())+(y-a.gety())*(y-a.gety()));
	}
	
}
	
