package g30126.Iakkel.Arthur.l3.e1;

import becker.robots.*;

public class E1 {

	   public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	      Thing parcel = new Thing(prague, 1, 3);
	      Robot karel = new Robot(prague, 1, 0, Direction.EAST);
	 
			// Direct the robot to the final situation
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.pickThing();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.putThing();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.pickThing();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.putThing();	   }
} 
