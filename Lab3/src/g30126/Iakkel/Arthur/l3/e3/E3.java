package g30126.Iakkel.Arthur.l3.e3;
import becker.robots.*;

public class E3 {
	
	   public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	      Robot karel = new Robot(prague, 1, 0, Direction.NORTH);
	 
			// Direct the robot to the final situation
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	// start turning right as three turn lefts
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	   }
} 

