package g30126.Iakkel.Arthur.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class E4 {
	 public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City ny = new City();
	      Wall blockAve0 = new Wall(ny, 1, 2, Direction.WEST);
	      Wall blockAve1 = new Wall(ny, 2, 2, Direction.WEST);
	      Wall blockAve2 = new Wall(ny, 2, 2, Direction.SOUTH);
	      Wall blockAve3 = new Wall(ny, 2, 3, Direction.SOUTH);
	      Wall blockAve4 = new Wall(ny, 2, 3, Direction.EAST);
	      Wall blockAve5 = new Wall(ny, 1, 3, Direction.EAST);
	      Wall blockAve6 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve7 = new Wall(ny, 1, 3, Direction.NORTH);
	      Robot karel = new Robot(ny, 0, 2, Direction.WEST);
	      
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	   }
}
