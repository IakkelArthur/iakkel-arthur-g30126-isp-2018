package g30126.Iakkel.Arthur.l4.e4;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class E4 {
	@Test
	public void testString() {
		Author a=new Author("Marcel","marcel@bizonu@.com",'M');
		assertEquals(a.getName()+" ("+a.getGender()+") at"+a.getEmail(),a.toString());
	}
}
