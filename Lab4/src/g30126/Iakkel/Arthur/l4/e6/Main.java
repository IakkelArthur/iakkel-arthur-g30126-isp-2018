package g30126.Iakkel.Arthur.l4.e6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30126.Iakkel.Arthur.l4.e6.Author;
import g30126.Iakkel.Arthur.l4.e6.Book;

public class Main {
	@Test
	public static void main(String[] args) {
		Author[] a=new Author[2];
		a[0]=new Author("gica","gica@gica.com",'F');
		a[1]=new Author("gica","gica@gica.com",'M');
		Book b=new Book("carte",a,5);
		assertEquals("carte by 2 authors",b.toString());
	}
}