package g30126.Iakkel.Arthur.l4.e7;

public class Cylinder extends Circle{
	private double height=1.0;
	
	Cylinder(){
	}
	
	Cylinder(double radius){
		super(radius);
	}
	
	Cylinder(double radius,double height){
		super(radius);
		this.height=height;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getArea() {
		return Math.PI*super.getRadius()*super.getRadius();
	}
	
	public double getVolume() {
		return getArea()*getHeight();
	}
}



