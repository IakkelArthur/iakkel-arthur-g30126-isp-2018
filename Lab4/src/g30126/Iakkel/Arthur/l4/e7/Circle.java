package g30126.Iakkel.Arthur.l4.e7;

public class Circle {
	private double radius;
	private String color;
	
	Circle(){
		this.radius=1.0;
		this.color="red";
	}
	
	Circle(double radius){
		this.radius=radius;
	}
	 
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return Math.PI*(radius*radius);
	}
	
	public String toString() {
		return color+" circle with radius "+radius;
	}
	
}
