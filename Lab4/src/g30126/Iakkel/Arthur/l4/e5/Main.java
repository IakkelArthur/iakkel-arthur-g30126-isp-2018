package g30126.Iakkel.Arthur.l4.e5;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import g30126.Iakkel.Arthur.l4.e5.Author;

public class Main {
	@Test	
	public void testtoString() {
		Author a1=new Author("john", "gica", 'M');
		Book b=new Book("carte",a1,5);
		assertEquals(b.getName()+"by"+a1.toString(),b.toString());
	}
}

