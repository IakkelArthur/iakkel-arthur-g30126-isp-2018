package e3;

import static org.junit.Assert.assertEquals;

public class Test {
	
	public void testGetArea() {	
		Circle c=new Circle();	
		assertEquals(Math.PI*c.getRadius(),c.getArea(),0.01);
	}
	 public static void main(String[] args) {
		 Test t=new Test();
		 t.testGetArea();
	 }
}
