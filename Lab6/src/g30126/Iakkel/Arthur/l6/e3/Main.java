package g30126.Iakkel.Arthur.l6.e3;
import java.awt.Color;

public class Main {
	public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90);
        s1.setId("abcd");
        s1.setCoord(100, 50);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.BLUE, 100);
        s2.setId("a");
        s2.setCoord(40, 70);
        s2.setFilled(true);
        b1.addShape(s2);
        Shape s3=new Rectangle(Color.GREEN, 50, 50);
        s3.setCoord(50, 80);
        s3.setFilled(true);
        s3.setId("b");
        b1.addShape(s3);
        b1.delteById("lala");
    }
}
