package g30126.Iakkel.Arthur.l6.e1;


import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90);
        s1.setId("a1");
        s1.setCoord(50, 50);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100);
        s2.setId("a2");
        s2.setCoord(50, 270);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 30, 100);
        s3.setCoord(100, 150);
        s3.setId("a3");
        s3.setFilled(true);
        b1.addShape(s3);
        
    }
}