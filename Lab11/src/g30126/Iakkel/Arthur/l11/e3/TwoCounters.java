package g30126.Iakkel.Arthur.l11.e3;

public class TwoCounters extends Thread{
	 int min,max;
 
     public TwoCounters(String nume)
     {
    	 super(nume);
    	 this.min=0;
    	 this.max=100;
     }
     
     public TwoCounters(String nume,int min,int max)
     {
    	 super(nume);
    	 this.min=min;
    	 this.max=max;
     }
     
     public void run(){
         for(int i=min;i<max;i++){
               System.out.println(getName() + " i = "+i);
               try {
                     Thread.sleep(100);
               } catch (InterruptedException e) {
                     e.printStackTrace();
               }
         }
         System.out.println(getName() + " job finalised.");
   }
}
