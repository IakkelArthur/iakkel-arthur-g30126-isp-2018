package g30126.Iakkel.Arthur.l11.e2;

import g30126.Iakkel.Arthur.l11.e3.TwoCounters;

public class Main {
	public static void main(String[] args) throws InterruptedException
	{
	      TwoCounters w1 = new TwoCounters("Proces 1");
	      TwoCounters w2 = new TwoCounters("Proces 2",101,200);
	      w1.start();
	      w1.join();
	      w2.start();
	}
}
