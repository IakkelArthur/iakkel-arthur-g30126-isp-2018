package g30126.Iakkel.Arthur.l5.e2;

public class ProxyImage implements Image {
	private Image image;
	   private String fileName,type;
	 
	   public ProxyImage(String fileName,String type){
	      this.fileName = fileName;
	      this.type=type;
	      switch (type) {
		case "real":
			image=new RealImage(fileName);
			break;
		case "rotated":
			image=(Image) new RotatedImage(fileName);
			break;
		}
	   }
	 
	   @Override
	   public void display() {
	      image.display();
	   }
	   public static void main(String[] args) {
		ProxyImage proxyImage=new ProxyImage("pere", "rotated");
		proxyImage.display();
	}
}
