package g30126.Iakkel.Arthur.l5.e1;

public class Rectangle extends Shape {
	 protected double width;
	 protected double lenght;

	   public Rectangle() {
	   }

	   public Rectangle(double width, double lenght) {
	       this.width = width;
	       this.lenght=lenght;
	   }

	   public Rectangle(String color, boolean filled, double width, double lenght) {
	       super(color, filled);
	       this.width=width;
	       this.lenght=lenght;
	   }

	   public double getWidth() {
	       return width;
	   }

	   public void setWidth(double width) {
	       this.width = width;
	   }

	   public double geLength() {
	       return lenght;
	   }

	   public void setLenght(double lenght) {
	       this.lenght = lenght;
	   }
	   public double getArea() {
			return lenght*width;
	   }
		public double getPerimeter() {
			return 2*(lenght+width);
		}

	   @Override
	   public String toString() {
	       return "Rectangle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", width=" + width +
	               ", lenght" + lenght +
	               '}';
	   }
}
