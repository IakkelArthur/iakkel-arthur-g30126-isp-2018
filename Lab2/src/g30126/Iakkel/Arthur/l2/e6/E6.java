package g30126.Iakkel.Arthur.l2.e6;

import java.util.Scanner;

public class E6 {
	static void f1(int n,int i,int f) {
		while(i<=n) {
			f=f*i;
			i=i+1;
		}
		System.out.println("Factorialul lui n este:"+f);
	}
	static void f2(int n,int i,int f) {
		Scanner in = new Scanner(System.in);
		if(i<=n) {
			f=f*i;
			i=i+1;
			f2(n,i,f);
		}
		else System.out.println("Factorialul lui n este:"+f);
	}
	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
		 System.out.print("introduceti n:");
	     int n = in.nextInt();
	     int i=1;
	     int f=1;
	     System.out.println("introduceti numarul functiei pe care doriti sa o folositi:");
	     System.out.println("     1=metoda nerecursiva"); 
	     System.out.println("     2=metoda recursiva");
	     int c = in.nextInt();
			if(c==1) {
				f1(n,i,f);
			}
			if(c==2) {
				f2(n,i,f);
			}
	}
}