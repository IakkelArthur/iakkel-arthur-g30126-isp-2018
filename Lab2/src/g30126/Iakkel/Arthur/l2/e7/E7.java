package g30126.Iakkel.Arthur.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class E7 {
	static void f(int x) {
		Scanner in = new Scanner(System.in);
		int k=0;
		for(int i=0;i<3;i++) {
			int j=i+1;
			System.out.println("Incercarea numarul:"+j);
			System.out.print("Ghiciti numarul:");
			int y = in.nextInt();
			if(y==x) {
				System.out.println("Ati ghicit numarul! Felicitari!");
				k=1;
				break;
			} 
			else if(y<x) System.out.println("Numarul este mai mic decat cel ales!");
			else if(y>x) System.out.println("Numarul este mai mare decat cel ales!");
		}
		if(k==0) {
			System.out.println("Ati pierdut!"
					+ "Nu ati reusit sa ghiciti numarul din 3 incercari!");	
			System.out.println("Numarul ales a fost:"+x);
		}
						
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Random r = new Random();
		int x=r.nextInt(30);
		System.out.println("Un numar intre 0 si 30 a fost ales!");
		f(x);
	}
}
	
