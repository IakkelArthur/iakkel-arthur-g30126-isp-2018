package g30126.Iakkel.Arthur.l2.e2;

import java.util.Scanner;

public class E2 {
		static void f1(int x){
			if(x==0){ 
				System.out.print("Zero");
			}
			else if(x==1){
				System.out.print("Unu");
			}
			else if(x==2){
				System.out.print("Doi");
			}
			else if(x==3){
				System.out.print("Trei");
			}
			else if(x==4){
				System.out.print("Patru");
			}
			else if(x==5){
				System.out.print("Cinci");
			}
			else if(x==6){
				System.out.print("Sase");
			}
			else if(x==7){
				System.out.print("Sapte");
			}
			else if(x==8){
				System.out.print("Opt");
			}
			else if(x==9){
				System.out.print("Noua");
			}
			else System.out.print("Nu e cifra");
		}
		
		static void f2(int x){
			String cifra;
			switch (x) {
			case 0:  cifra="Zero";
					 break;
            case 1:  cifra= "Unu";
                     break;
            case 2:  cifra = "Doi";
                     break;
            case 3:  cifra = "Trei";
                     break;
            case 4:  cifra = "Patru";
                     break;
            case 5:  cifra = "Cinci";
                     break;
            case 6:  cifra = "Sase";
                     break;
            case 7:  cifra = "Sapte";
                     break;
            case 8:  cifra = "Opt";
                     break;
            case 9:  cifra = "Noua";
                     break;
            default: cifra = "Nu e cifra";
                     break;
        }
        System.out.println(cifra);
		}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("introduceti o cifra:");
		int x = in.nextInt();
		System.out.print("introduceti numarul functiei pe care doriti sa o folositi:");
		int c = in.nextInt();
		if(c==1) {
			f1(x);
		}
		if(c==2) {
			f2(x);
		}
	}
}
