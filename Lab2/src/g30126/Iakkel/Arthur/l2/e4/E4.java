package g30126.Iakkel.Arthur.l2.e4;

import java.util.Random;
import java.util.Scanner;

public class E4 {
	static void f(int n) {
		Scanner in = new Scanner(System.in);
		int[] a = new int[n];
		int max = 0;
		Random r = new Random();
		for(int i=0;i<n;i++){
			a[i]=r.nextInt(100);
		}
		for(int i=0;i<n;i++){
			if(a[i]>max) {
				max=a[i];
			}
		}
		System.out.println("Maximul din vector este:"+max);
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("introduceti lungimea vectorului:");
		int n = in.nextInt();
		f(n);
	}
}
